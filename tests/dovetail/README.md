# Dovetail - libvirt test framework based on Avocado

The Dovetail aims to be an alternative test framework for the libvirt project
using as muscles only Avocado and python-libvirt. This can be used to write
functional and integration tests and it is inspired on the libvirt-tck
framework, but instead of Perl it is written in Python.

The idea is to provide to the test writers helper classes to avoid boilerplate
code, improve code readability and maintenance.

## Disclaimer

**For now, this framework is assuming that you are running the tests in a clean
and fresh environment. Although we take care when creating and removing domains
and networks, be aware that these tests may affect your system.**

For future versions we have plans to integrate this with `lcitool` in order to
prepare the environment before executing the tests.

## Requirements

The libvirt interactions will all be done via the libvirt Python bindings
(libvirt-python), we also rely on Avocado, not only to execute the tests but
also to generate multiple output format artifacts (that can be uploaded to CI
environments) and also the image fetch, so we don't need to handle with local
cache, checksum and any other test's requirement task.

In order to install the requirements, please execute the following command:

```bash
  $ pip3 install -r requirements.txt
```

If you wish you can run this command under a virtual environment.

## Features

 * Parallel execution: If your tests are independent of each other, you can run
   those in parallel, when getting images, Avocado will create a snapshots of
   those images for tests to avoid conflicts. Also domains created by tests have
   unique names to avoid collisions.

 * Creating domains easily: You can use the `.create_domain()` method to create
   a generic domain which is based on a Jinja2 template. If you want to use an
   already existing XML domain description, no problem, use
   `Domain.from_xml_path()` instead.

 * Multiple test result formats: TAP, HTML, JSON, xUnit (Gitlab CI ready).

 * Test tags: Ability to mark tests with tags, for filtering during execution
   and/or results.

 * Different isolation models: By default the avocado runner will spawn subprocess
   for each test, but we can configure it for specific use cases applying different
   isolation models to tests, for example, containers.

 * Requirements resolver: Avocado has implemented the 'requirements resolver'
   feature that makes it easy to define test requirements. Currently, the
   RequirementsResolver can handle `assets` (any local or remote files) and
   `packages` but new requirements are on the roadmap, including `ansible
   playbooks`. For now, it is still only an experimental feature though.

 * Test result artifacts for future analysis: you can use `avocado
   jobs` command or inspect your `~/avocado/job-results/<job-id>/test-results` folder
   for more detailed results.

 * Multiple test execution with variants: the variants subsystem is what allows
   the creation of multiple variations of parameters, and the execution of
   tests with those parameter variations.

 * Clear separation between tests and bootstrap stages: If something fails
   during the setUp() metod execution, your test will be not marked as FAIL,
   instead it will be flagged as ERROR. You can also use some decorators
   (@cancel_on, @skipUnless, ...) around your test to avoid false positves.

## Running

Before running the tests, you need to make sure that `libvirtd` service is
running and qemu binary is located in `/usr/bin/qemu-system-x86_64`

If you are on s390x you need to change the vmimage config in
`tests/dovetail/dovetail/defaults.py` like this:
```diff
--- a/tests/dovetail/dovetail/defaults.py
+++ b/tests/dovetail/dovetail/defaults.py
@@ -6,7 +6,7 @@ TEMPLATE_PATH = {
 }

 VMIMAGE = {
-    "provider": "fedora",
+    "provider": "FedoraSecondary",
     "version": "39",
     "checksum": "cd4200e908280ef993146a06769cf9ea8fda096a",
 }
```

After installing the requirements, you can run the tests with the following
commands:

```bash
  $ export PYTHONPATH=pwd:$PYTHONPATH
  $ avocado run ./tests/domain/*.py
```

Or, if you want to test your local changes, you can run it with `run script`
from build tree:

```bash
  # The meson setup build configuration can differ based on you environment.
  # More info in https://libvirt.org/compiling.html#configuring-the-project

  # from the libvrt root dir
  $ meson setup build -Dsystem=true -Ddriver_qemu=enabled -Ddriver_libvirtd=enabled -Ddriver_remote=enabled
  $ cd build
  $ meson compile
  $ cd ../tests/dovetail/
  $ export PYTHONPATH=pwd:$PYTHONPATH
  $ ../../build/run avocado run ./tests/domain/*.py
```

## Writing Tests

You can write your tests here the same way you write for the [Avocado
Framework](https://avocado-framework.readthedocs.io/en/latest/).
The Dovetail is build on top of Avocado "instrumented tests" (Python tests).

See the `tests/` folder for some references and ideas. In addition, feel free
to read the [Avocado Test Writer’s
Guide](https://avocado-framework.readthedocs.io/en/latest/guides/writer/chapters/writing.html) to
play with some advanced features of the framework.
